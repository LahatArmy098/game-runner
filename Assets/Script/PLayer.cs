﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PLayer : MonoBehaviour
{
    public float speed = 5f;
    Animator animator;
    int isRunHash;
    public Rigidbody rb;
    private Vector3 currentPosition;
    public float xDirection;

    public Toggle toggler;

    public AudioSource audio;

    public bool running;
    public bool moveRight;
    public bool moveLeft;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        isRunHash = Animator.StringToHash("B_Run");

        toggler.onValueChanged.AddListener(delegate
        {
            ToggleValueChangedOccured(toggler);
        });
        
    }

    // Update is called once per frame
    void Update()
    {
        xDirection = Input.GetAxis("Horizontal");
        rb.velocity = new Vector3(xDirection, 0.0f, 0.0f) * speed;
        if (xDirection >= 0.1f)
        {
            transform.eulerAngles = new Vector3(0, 90, 0);
            
        }
        if (xDirection <= -0.1f)
        {
            transform.eulerAngles = new Vector3(0, -90, 0);
        }

        running = animator.GetBool(isRunHash);
        moveRight = Input.GetKey(KeyCode.RightArrow);
        moveLeft = Input.GetKey(KeyCode.LeftArrow);

        if (!running && moveRight)
        {
            animator.SetBool(isRunHash, true);
            audio.Play();
        }
        if (running && !moveRight && !moveLeft)
        {
            animator.SetBool(isRunHash, false);
            audio.Stop();
        }

        if (!running && moveLeft)
        {
            animator.SetBool(isRunHash, true);
            audio.Play();
        }
    }


    void ToggleValueChangedOccured(Toggle tglValue)
    {
        if (tglValue.isOn)
        {
            animator.speed = 1.5f;
            speed = 7f;
            
        }
        else
        {
            animator.speed = 1f;
            speed = 5f;
        }
    }

    public void win()
    {
        animator.SetTrigger("IsFinish");
    }

}
