﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MovePlat : MonoBehaviour
{
    public GameObject p2,exit,player2;
    public Transform target;
    public float rotateTime = 3.0f;
    public float rotateDegrees = 90.0f;
    private bool rotating = false;

    private void Start()
    {
    }

    void Update()
    {
        //if (PLayer.xDirection == 1 && !rotating)
        //{
        //    StartCoroutine(Rotate(transform, target, Vector3.forward, -rotateDegrees, rotateTime));
        //}

        //if (PLayer.xDirection == -1  && !rotating)
        //{
        //    StartCoroutine(Rotate(transform, target, Vector3.forward, rotateDegrees, rotateTime));
        //}
    }

    private void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.CompareTag("Player"))
        {

            //StartCoroutine(waitSpawn());
            //player.gameObject.tag = "Finish";
            //StartCoroutine(Rotate(p2.transform, target, Vector3.forward, -rotateDegrees, rotateTime));

            if (player2.GetComponent<PLayer>().xDirection > 0.1f  && !rotating)
            {
                StartCoroutine(Rotate(p2.transform, target, Vector3.forward, -rotateDegrees, rotateTime));
            }

            if (player2.GetComponent<PLayer>().xDirection < -0.1f && !rotating)
            {
                StartCoroutine(Rotate(p2.transform, target, Vector3.forward, rotateDegrees, rotateTime));
            }
        }

    }

    //IEnumerator waitSpawn()
    //{
    //    yield return new WaitForSeconds(1.0f);
    //    gameObject.SetActive(false);
    //    exit.SetActive(true);
    //}

    private IEnumerator Rotate(Transform camTransform, Transform targetTransform, Vector3 rotateAxis, float degrees, float totalTime)
    {
        if (rotating)
            yield return null;
        rotating = true;

        Quaternion startRotation = camTransform.rotation;
        Vector3 startPosition = camTransform.position;
        // Get end position;
        p2.transform.RotateAround(targetTransform.position, rotateAxis, degrees);
        Quaternion endRotation = camTransform.rotation;
        Vector3 endPosition = camTransform.position;
        camTransform.rotation = startRotation;
        camTransform.position = startPosition;

        float rate = degrees / totalTime;
        //Start Rotate
        for (float i = 0.0f; Mathf.Abs(i) < Mathf.Abs(degrees); i += Time.deltaTime * rate)
        {
            camTransform.RotateAround(targetTransform.position, rotateAxis, Time.deltaTime * rate);
            yield return null;
        }

        camTransform.rotation = endRotation;
        camTransform.position = endPosition;
        rotating = false;
    }
}
