﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public GameObject panel1, panel2;
    public AudioSource audio1, audio2;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            audio1.Stop();
            audio2.Play();
            Destroy(panel1.gameObject);
            Destroy(panel2.gameObject);
        }
    }
}
