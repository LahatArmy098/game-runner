﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    public GameObject player, win, exit;
    public AudioSource audio;

    private void Start()
    {
        win.SetActive(false);
        exit.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player.GetComponent<PLayer>().win();
            player.GetComponent<PLayer>().speed = 0;
            player.GetComponent<PLayer>().running = true;
            audio.Play();
            StartCoroutine(waitWin());
        }
    }

    IEnumerator waitWin()
    {
        yield return new WaitForSeconds(0.5f);
        player.GetComponent<PLayer>().enabled = false;
        win.SetActive(true);
        exit.SetActive(true);
    }

    public void quitGames()
    {
        Application.Quit();
    }
}
